package srv

type Navigator struct {
}

func (n *Navigator) Resolve(version int, typeOf string) (bool, interface{}) {
	return true, nil
}

func (n *Navigator) Register(version int, typeOf string, cli []interface{}) error {
	return nil
}
